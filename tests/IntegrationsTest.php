<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
        $body = $response->getBody()->getContents();

        $dinos = getdinos(); //donne fonction à la var dinos

        foreach($dinos as $dino){
            $this->assertStringContainsString($dino->name, $body); //test si dans la page index se trouve bien le nom des dinos
            $this->assertStringContainsString($dino->avatar, $body); //pareil mais pour leur avatar
        }
    }

    public function test_dinosaur() //test pour la page /dinosaur/@name
    {
        $response = $this->make_request("GET", "/dinosaur/brachiosaurus");
        $dino = getUnDino('brachiosaurus'); //fonction getUnDino dans la var dino
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]); 
        $body = $response->getBody()->getContents();
 
        $this->assertStringContainsString($dino->name, $body); //test si se trouve bien les nom et avatar des dinosaurs (récup par api)
        $this->assertStringContainsString($dino->avatar, $body);
    }

}