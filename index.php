<?php require "vendor/autoload.php";

// CSS : les fichiers sont dans le dossier assets. Comme ça qu'on charge le fichier CSS et après 
// à mettre dans le fichier html/twig 

// Configuration de Twig (traduction de jinja pour PHP)
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/view');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

//Enregistrement de Twig en temps qu'extention de Flight
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
});

//Configuration des routes avec Flight
Flight::route('/', function(){
    $data =[
        'dinos' => getdinos(), // data pour appeler la fonction où on stock l'api pour pouvoir l'afficher dans la boucle for
    ];
    Flight::view()->display('base.twig',$data);
});

Flight::route('/dinosaur/@name', function(){
    $data =[
        'MyDino' => getUnDino(),  // data pour appeler la fonction où on stock l'api pour pouvoir l'afficher dans la boucle for
    ];
    Flight::view()->display('detaildino.twig',$data);
    
});

Flight::start(); 