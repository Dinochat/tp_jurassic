<?php

function getdinos()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    $dinos = json_decode($response->body); 
    return $dinos;
}

function getUnDino()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/brachiosaurus');
    $MonDino = json_decode($response->body);
    return $MonDino;
}

use Michelf\Markdown;

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}
